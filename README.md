squid
-----

*Stay Fresh!*

Build Instructions
==================

You must have:

  - Vagrant

That's it! Run `vagrant up` in the repository root and automatically,
a development VM will be set up with you with a GCC cross compiler pre-built
and waf preinstalled for building. Note that the entire setup procedure may
take a while when first up'd, but if the VM is shut down using `vagrant halt`
between uses, the provision script should never be re-run and you should have
quick startup times.

The VM that Vagrant will build for you requires:

  - 1GB of RAM
  - (Soon) A processor capable of running 64bit guests

To access the build environment, use `vagrant ssh`.

From within the build environment, first run `waf configure` to perform
pre-build configuration, and then run `waf iso` to generate an ISO (Not yet
implemented), or `waf kernel` to build a kernel binary. Use `waf distclean` to
clean up any generated files.