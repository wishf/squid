#!/usr/bin/env bash

apt-get update
apt-get install -y libgmp-dev libmpfr-dev libisl-dev libcloog-isl-dev libmpc-dev texinfo gcc make flex bison g++ binutils

cd /home/vagrant

# Download source
mkdir source
cd source
echo "Downloading GCC 5.2.0 source"
curl -s -o gcc-5.2.0.tar.gz ftp://ftp.gnu.org/gnu/gcc/gcc-5.2.0/gcc-5.2.0.tar.gz
tar xzf gcc-5.2.0.tar.gz
echo "Downloading Binutils 2.25 source"
curl -s -o binutils-2.25.tar.gz ftp://ftp.gnu.org/gnu/binutils/binutils-2.25.tar.gz
tar xzf binutils-2.25.tar.gz

# Export prep variables
export PREFIX="/home/vagrant/opt/cross"
# TODO: Change to be x86-64
export TARGET=i686-elf
export PATH="$PREFIX/bin:$PATH"

# Build binutils
echo "Building Binutils"
mkdir build-binutils
cd build-binutils
../binutils-2.25/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
make
make install
cd ..

# Build gcc
echo "Building GCC"
mkdir build-gcc
cd build-gcc
../gcc-5.2.0/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers
make all-gcc
make all-target-libgcc
make install-gcc
make install-target-libgcc
cd ..

cd ..

# Setup build environment
echo "Fetching build tools"
mkdir buildtools
cd buildtools
curl -s -o waf https://waf.io/waf-1.8.12
chmod +x waf
cd ..

# Change ownership of directories
chown -R vagrant:vagrant source
chown -R vagrant:vagrant buildtools

# Build vagrant user's bashrc
cat << EOT >> /home/vagrant/.bashrc
# Export cross compiler and build tools into path
export PATH="/home/vagrant/buildtools:/home/vagrant/opt/cross/bin:$PATH"
EOT